//Plateau Contenant l'etat du jeu
/*
00=Vide
01=pion blanc
02=tour blanc
03=cavalier blanc
04=fou blanc
05=reine blanc
06=roi blanc

11=pion noir
12=tour noir
13=cavalier noir
14=fou noir
15=reine noir
16=roi noir
*/
class Plateau {
    constructor(){
        this.plateau = new Array(8);
        for (let i=0;i<8;i++){
            this.plateau[i]=new Array(8);
        }

        this.initialisation();
    }

    initialisation(){
        for(let i=0;i<8;i++){
            for(let j=0;j<8;j++){

                this.plateau[i][j]=0;

                if(i==1){this.plateau[i][j]=11;}//On place la ligne des pions noirs
                if(i==0){
                    if(j==0 ||j==7){this.plateau[i][j]=12;}
                    if(j==1 || j==6){this.plateau[i][j]=13;}
                    if(j==2 || j==5){this.plateau[i][j]=14;}
                    if(j==3){this.plateau[i][j]=16;}
                    if(j==4){this.plateau[i][j]=15;}
                }

                if(i==6){this.plateau[i][j]= 1;}
                if(i==7){
                    if(j==0 ||j==7){this.plateau[i][j]=2;}
                    if(j==1 || j==6){this.plateau[i][j]=3;}
                    if(j==2 || j==5){this.plateau[i][j]=4;}
                    if(j==3){this.plateau[i][j]=6;}
                    if(j==4){this.plateau[i][j]=5;}
                }
            }
        }
    }

    affichage(){
        console.log(this.plateau);
    }
}